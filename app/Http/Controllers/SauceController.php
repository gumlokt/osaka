<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SauceController extends Controller
{
    public function sauces()
    {
        return view('menu.sauces.sauces');
    }
}
