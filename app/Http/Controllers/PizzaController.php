<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PizzaController extends Controller
{
    public function pizza()
    {
        return view('menu.pizza.pizza');
    }

    public function ingredientsforpizza()
    {
        return view('menu.pizza.ingredientsforpizza');
    }
}
