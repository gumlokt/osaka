<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StreetfoodController extends Controller
{
    public function streetfood()
    {
        return view('menu.streetfood.streetfood');
    }

}
