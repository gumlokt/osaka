<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SaladController extends Controller
{
    public function salads()
    {
        return view('menu.salads.salads');
    }
}
