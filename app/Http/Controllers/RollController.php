<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RollController extends Controller
{
    public function coldrolls()
    {
        return view('menu.rolls.coldrolls');
    }
    
    public function friedandbakedrolls()
    {
        return view('menu.rolls.friedandbakedrolls');
    }
    
    public function brandedrolls()
    {
        return view('menu.rolls.brandedrolls');
    }
    
    public function sets()
    {
        return view('menu.rolls.sets');
    }
    


    public function allforrolls()
    {
        return view('menu.rolls.allforrolls');
    }

}
