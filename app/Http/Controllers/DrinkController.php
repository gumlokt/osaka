<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DrinkController extends Controller
{
    public function drinks()
    {
        return view('menu.drinks.drinks');
    }
}
