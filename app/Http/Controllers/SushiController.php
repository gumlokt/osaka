<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SushiController extends Controller
{
    public function sushi()
    {
        return view('menu.sushi.sushi');
    }

    public function allforsushi()
    {
        return view('menu.sushi.allforsushi');
    }

}
