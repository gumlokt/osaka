@extends('layouts.app')

@section('content')
    <div class="row border-bottom border-danger" style="padding-top: 2em; padding-bottom: 2em;">
        <div class="card text-white bg-danger">
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-6 text-center text-lg-center">
                        <img src="/images/logo_black_svg3.svg" class="img-fluid" alt="ads">
                        <h3 style="padding-bottom: 0.2em;">РЕСТОРАН ДОСТАВКИ</h3>
                        <h2 style="padding-bottom: 0.2em;">РЕЖИМ РАБОТЫ с 10:00 до 22:00</h2>
                        <h2 style="padding-bottom: 0.2em;">ПО ПЯТНИЦАМ с 09:00 до 22:00</h2>
                        <h4 style="padding-bottom: 0.2em;">Адрес: г. Губкинский, мкрн. 2</h4>
                        <h5 style="padding-bottom: 0.2em;">Справа от здания ДУМИ</h5>
                        <h4>Наличный и безналичный расчет</h4>
                        <h2 style="color: yellow;">ОБМЕНЯЙ ЧЕКИ НА СКИДКУ</h2>
                    </div>

                    <div class="col-lg-6 text-center text-lg-center">
                        <h1 class="display-4"><strong>+7 (34936) 6-42-06</strong></h1>
                        <h1 class="display-4"><strong>+7 (922) 457-66-77</strong></h1>
                        <a href="https://vk.com/club188867405" title="Наша страничка в VK" target="_blank">
                            <img src="/images/qr.png" alt="qr-code" style="width: 30%; margin: 1em 0 1em 1em;">
                        </a>
                        <img src="/images/discont-card.jpg" alt="discont-card" style="width: 60%; margin: 3em 0 3em 1em;">
                        <h4 style="color: yellow;">НА САМОВЫВОЗ СКИДКА 20%</h4>
                        <h4 style="color: yellow;">ИМЕНИННИКАМ СКИДКА 20%</h4>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row border-bottom border-danger" style="padding-top: 2em; padding-bottom: 2em;">
        <div class="col-lg-12 text-center text-lg-center">
            <h6>Уважаемые клиенты!</h6>
            <p style="margin: 0;">Из-за роста цен на сырье мы вынуждены включать стоимость упаковки в цену заказов</p>
            <p style="margin: 0;">Если заказ сформирован из раздела "Роллы", то стоимость упаковки составит <span class="text-danger">90 р.</span></p>
            <p style="margin: 0;">Если заказ сформирован из разделов "Пицца" и "Роллы", то стоимость упаковки составит <span class="text-danger">180 р.</span></p>
            <p style="margin: 0;">Если заказ сформирован из разделов "Пицца", "Роллы" и любоого другого раздела, то стоимость упаковки составит <span class="text-danger">270 р.</span></p>
            <p style="margin: 0;">При заказе бургера, Чикен Дога или шаурмы стоимость упаковки составит <span class="text-danger">35 р.</span></p>
            <p style="margin: 0;">При заказе от 5 пицц и более стоимость упаковки составит <span class="text-danger">400 р.</span></p>
            <p style="margin: 0;">Расчитываем на Ваше понимание</p>
            <br>
            <p style="margin: 0; color: #f00;">Внешний вид блюд может отличаться от представленного на фотографиях</p>
        </div>
    </div>

    <div class="row" style="padding-top: 2em; padding-bottom: 2em;">
        <div class="col-lg-6 text-left text-lg-left">
            <p>
                <span class="badge badge-danger">&nbsp;&nbsp;</span>
                Все цены на сайте указаны в российских рублях и носят информационный характер<br>
                <small style="margin-left: 20px;">(точные цены уточняйте при заказе блюд)</small>
            </p>
            <p>
                <span class="badge badge-info">&nbsp;&nbsp;</span>
                В состав одной порции ролла входят:<br>
                <span style="margin-left: 40px;">15 граммов соевого соуса</span><br>
                <span style="margin-left: 40px;">15 грамм имбиря</span><br>
                <span style="margin-left: 40px;">&nbsp;&nbsp;5 грамм васаби</span><br>
                <small style="margin-left: 20px;">(дополнительные порции соевого соуса, имбиря и васаби доступны за <a href="{{ url('allforrolls') }}">отдельную плату</a>)</small>
            </p>
            <p>
                <span class="badge badge-info">&nbsp;&nbsp;</span>
                Внешний вид блюд может отличаться от представленного на фотографиях
            </p>
            <p>
                <span class="badge badge-success">&nbsp;&nbsp;</span>
                Приготовление блюд из высококачественных продуктов - главный принцип нашей работы
            </p>
            <p>
                <span class="badge badge-success">&nbsp;&nbsp;</span>
                Заказанные блюда рекомендуется употребить в течение 12 часов после доставки
            </p>
            <p>
                <span class="badge badge-warning">&nbsp;&nbsp;</span>
                Вы можете обменять собранные чеки с заказов на скидочную карту:<br>
                <span style="margin-left: 40px;">Чеки на сумму от 8 000 руб. - скидочная карта на 5%</span><br>
                <span style="margin-left: 40px;">Чеки на сумму от 10 000 руб. - скидочная карта на 7%</span><br>
                <span style="margin-left: 40px;">Чеки на сумму от 20 000 руб. - скидочная карта на 10%</span><br>
                <span style="margin-left: 40px;">Чеки на сумму от 100 000 руб. - скидочная карта на 15%</span><br>
                <span style="margin-left: 40px;">Чеки на сумму от 200 000 руб. - скидочная карта на 20%</span>
            </p>
            <p>
                <span class="badge badge-warning">&nbsp;&nbsp;</span>
                Вы можете приобрести подарочные сертификаты:<br>
                <span style="margin-left: 40px;">Номинал 1 000 руб. - со скидкой 10%</span><br>
                <span style="margin-left: 40px;">Номинал 2 000 руб. - со скидкой 10%</span><br>
                <span style="margin-left: 40px;">Номинал 3 000 руб. - со скидкой 10%</span>
            </p>
        </div>


        <div class="col-lg-6 text-left text-lg-left">
            <p><span class="badge badge-info">&nbsp;&nbsp;</span> Доставка</p>

            <table class="table" style="margin-left: 20px;">
                <thead>
                    <tr><th>Населенный пункт</th>
                    <th>Условия и цена доставки</th>
                </tr></thead>
                <tbody>
                    <tr>
                        <td>г. Губкинский</td>
                        <td>
                            100 руб.<br>
                            <span class="text-danger">*</span> При заказе от 450 руб. - <span class="text-danger">бесплатно</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            7, 8 панель<br>
                            п. Энергетиков<br>
                            УТТ<br>
                            ГПЗ<br>
                            Молокозавод<br>
                        </td>
                        <td>
                            150 руб.<br>
                            <span class="text-danger">*</span> При заказе от 3000 руб. - <span class="text-danger">бесплатно</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            АЗС "Газпром"<br>
                            Северная экспедиция<br>
                        </td>
                        <td>200 руб.</td>
                    </tr>
                    <tr>
                        <td>дачный поселок</td>
                        <td>
                            250 руб.<br>
                            <small class="text-danger">только по предоплате</small>
                        </td>
                    </tr>
                    <tr>
                        <td>РН-Снабжение</td>
                        <td>заказ от 2000 руб.<br>
                            <small class="text-danger">только по предоплате</small>
                        </td>
                    </tr>
                    <tr>
                        <td>п. Пурпе</td>
                        <td>заказ от 4500 руб.<br>
                            <small class="text-danger">только по предоплате</small>
                        </td>
                    </tr>
                    <tr>
                        <td>п. Пурпе-1</td>
                        <td>
                            заказ от 4500 руб.<br>
                            <small class="text-danger">только по предоплате</small>
                        </td>
                    </tr>
                </tbody>
            </table>

            <p>
                <span class="badge badge-warning">&nbsp;&nbsp;</span>
                <span class="text-danger">СКИДКИ НЕ ДЕЙСТВУЮТ:</span><br>
                <span style="margin-left: 40px;">На весь ассортимент напитков</span><br>
                <span style="margin-left: 40px;">На Бизнес обед</span><br>
                <span style="margin-left: 40px;">На Фаст-фуд</span><br>
                <span style="margin-left: 40px;">На Шашлык</span><br>
                <span style="margin-left: 40px;">На Коробочки WOK</span>
            </p>
        </div>

    </div>
@endsection
