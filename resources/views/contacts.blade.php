@extends('layouts.app')

@section('content')
<div class="row" style="padding-top: 2em;">
    <div class="col-lg-6">
        <h3>Контактная информация</h3>

        <table class="table">
            <tbody>
                <tr>
                    <th scope="row"><i class="fas fa-flag fa-fw"></i> Наименование:</th>
                    <td>Ресторан доставки "Осака"</td>
                </tr>
                <tr>
                    <th scope="row"><i class="fas fa-map-marker-alt fa-fw"></i> Почтовый адрес:</th>
                    <td>
                        Российская Федерация, 629830,<br>
                        Ямало-Ненецкий автономный округ,<br>
                        г. Губкинский, микрорайон 2<br>
                        Справа от здания ДУМИ
                    </td>
                </tr>
                <tr>
                    <th scope="row"><i class="fas fa-globe fa-fw"></i> Сайт в интернет:</th>
                    <td>http://www.osaka89.ru/</td>
                </tr>
                <tr>
                    <th scope="row"><i class="fas fa-phone fa-fw"></i> Телефон:</th>
                    <td>+7 (34936) 6-42-06<br></td>
                </tr>
                <tr>
                    <th scope="row"><i class="fas fa-user-secret fa-fw"></i> Директор:</th>
                    <td>Бакишев Тимур Анатольевич</td>
                </tr>
                <tr>
                    <th scope="row"><i class="fas fa-user-alt fa-fw"></i> Шеф повар:</th>
                    <td>Бакишев Тимур Анатольевич</td>
                </tr>

                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>


    <div class="col-lg-6">
        <h3>Отправьте нам сообщение</h3>

        <form action="{{ url('sendmessage') }}" method="POST">
            @csrf

            <div class="form-group row">
                <label for="name" class="col-sm-3 col-form-label"><span class="text-danger">*</span> Ваше имя:</label>
                <div class="col-sm-9">
                    <input type="text" name="name" class="form-control" placeholder="Ваше имя" id="name">
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-sm-3 col-form-label"><span class="text-danger">*</span> Ваш E-mail:</label>
                <div class="col-sm-9">
                    <input type="email" name="email" class="form-control" placeholder="Ваш E-mail" id="email">
                </div>
            </div>

            <div class="form-group row">
                <label for="name" class="col-sm-3 col-form-label">Тема:</label>
                <div class="col-sm-9">
                    <input type="text" name="subject" class="form-control" placeholder="Тема" id="subject">
                </div>
            </div>

            <div class="form-group row">
                <label for="message" class="col-sm-3 col-form-label"><span class="text-danger">*</span> Сообщение:</label>
                <div class="col-sm-9">
                    <textarea name="message" class="form-control" rows="3" id="message"></textarea>
                </div>
            </div>


            <div class="form-group row">
                <div class="col-sm-9 offset-sm-3">
                    <button type="submit" class="btn btn-light"><i class="far fa-envelope"></i> Отправить</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

