<nav class="navbar fixed-top navbar-expand-xl navbar-dark box-shadow" style="background-color: red;">
    <div class="container">
        <a class="navbar-brand active" href="/">
            <i class="fas fa-home fa-fw"></i>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item{{ ends_with(url()->current(), url('news')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('news') }}" style="color: balck;">Новинки</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('businesslunch')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('businesslunch') }}">Обед</a>
                </li>

                <li class="nav-item dropdown{{ ends_with(url()->current(), url('sushi')) ? ' active' : ends_with(url()->current(), url('allforsushi')) ? ' active' : '' }}">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Суши
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item{{ ends_with(url()->current(), url('sushi')) ? ' active' : '' }}" href="{{ url('sushi') }}">Суши</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item{{ ends_with(url()->current(), url('allforsushi')) ? ' active' : '' }}" href="{{ url('allforsushi') }}">Все для суши</a>
                    </div>
                </li>

                <li class="nav-item dropdown{{ ends_with(url()->current(), url('coldrolls')) ? ' active' : ends_with(url()->current(), url('friedandbakedrolls')) ? ' active' : ends_with(url()->current(), url('brandedrolls')) ? ' active' : ends_with(url()->current(), url('sets')) ? ' active' : ends_with(url()->current(), url('allforrolls')) ? ' active' : '' }}">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Роллы
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item{{ ends_with(url()->current(), url('coldrolls')) ? ' active' : '' }}" href="{{ url('coldrolls') }}">Холодные</a>
                        <a class="dropdown-item{{ ends_with(url()->current(), url('friedandbakedrolls')) ? ' active' : '' }}" href="{{ url('friedandbakedrolls') }}">Жаренные и запеченные</a>
                        <a class="dropdown-item{{ ends_with(url()->current(), url('brandedrolls')) ? ' active' : '' }}" href="{{ url('brandedrolls') }}">Фирменные роллы</a>
                        <a class="dropdown-item{{ ends_with(url()->current(), url('sets')) ? ' active' : '' }}" href="{{ url('sets') }}">Сеты</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item{{ ends_with(url()->current(), url('allforrolls')) ? ' active' : '' }}" href="{{ url('allforrolls') }}">Все для роллов</a>
                    </div>
                </li>

                <li class="nav-item dropdown{{ ends_with(url()->current(), url('pizza')) ? ' active' : ends_with(url()->current(), url('ingredientsforpizza')) ? ' active' : '' }}">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Пицца
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item{{ ends_with(url()->current(), url('pizza')) ? ' active' : '' }}" href="{{ url('pizza') }}">Пицца</a>
                        <div class="dropdown-divider"></div>
                        <!-- <a class="dropdown-item{{ ends_with(url()->current(), url('ingredientsforpizza')) ? ' active' : '' }}" href="{{ url('ingredientsforpizza') }}">Дополнительные ингридиенты</a> -->
                        <a class="dropdown-item{{ ends_with(url()->current(), url('ingredientsforpizza')) ? ' active' : '' }}" href="{{ url('ingredientsforpizza') }}">Ингридиенты к пицце</a>
                    </div>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('boxes')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('boxes') }}">Коробочки</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('shashlik')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('shashlik') }}">Шашлык</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('pasta')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('pasta') }}">Паста</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('streetfood')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('streetfood') }}">Стрит-фуд</a>
                </li>


                <li class="nav-item{{ ends_with(url()->current(), url('salads')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('salads') }}">Салаты</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('pies')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('pies') }}">Пироги</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('sauces')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('sauces') }}">Соусы</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('drinks')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('drinks') }}">Напитки</a>
                </li>

                <li class="nav-item{{ ends_with(url()->current(), url('actions')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('actions') }}">Акции</a>
                </li>
            </ul>

            <ul class="navbar-nav mr-right">
                <li class="nav-item{{ ends_with(url()->current(), url('contacts')) ? ' active' : '' }}">
                    <a class="nav-link" href="{{ url('contacts') }}">Контакты</a>
                </li>
            </ul>

        </div>
    </div>
</nav>
