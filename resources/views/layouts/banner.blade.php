<div class="row border-bottom border-danger">
    <div class="col-md-4 text-center text-md-left">
        <p>
            <i class="fas fa-mobile-alt fa-fw"></i> +7 (922) 457-66-77<br>
            <i class="fas fa-phone fa-fw"></i> +7 (34936) 6-42-06
        </p>
        <p>
            <i class="fas fa-truck fa-fw"></i> Бесплатная доставка<br>
            <em><small><i class="fas fa-exclamation-triangle fa-fw"></i> <span class="text-danger">при заказе от 450 руб.</span></small></em>
        </p>
    </div>

    <div class="col-md-4 text-center text-md-center">
        <p><img src="{{ url('images/logo_black_svg1.svg') }}" class="img-fluid" alt="Responsive image"></p>
    </div>

    <div class="col-md-4 text-center text-md-right">
        <p>
            <i class="far fa-clock fa-fw"></i> Режим работы: 10:00 - 22:00<br>
            <i class="far fa-clock fa-fw"></i> Пятница: 09:00 - 22:00
        </p>
        <p>
            <i class="fas fa-map-marker-alt fa-fw"></i> г. Губкинский, мкрн. 2<br>
            <em><small><i class="fas fa-exclamation-triangle fa-fw"></i> <span class="text-danger">Справа от здания ДУМИ</span></small></em>
        </p>
    </div>
</div>
