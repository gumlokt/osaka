        <div class="container">
            <div class="row">
                <div class="col-sm-3 text-center text-sm-left">
                    <span class="text-white-50"><small>© Osaka, 2013-{{ date('Y') }}</small></span>
                </div>
                <div class="col-sm-6 text-center text-sm-center">
                    <span class="text-danger"><small style="font-weight: 100;">developed by ivkakupshev@yandex.ru</small></span>
                </div>
                <div class="col-sm-3 text-center text-sm-right">
                    <a href="{{ url('contacts') }}" class="text-primary" id="contacts"><small><i class="far fa-envelope"></i> Контакты</small></a>
                </div>
            </div>
        </div>
