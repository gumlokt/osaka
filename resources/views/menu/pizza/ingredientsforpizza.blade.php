@extends('layouts.app')

@section('content')
    <div class="row" style="padding-top: 1em;">
        <div class="col text-center">
            <h1 class="display-4">Дополнительные ингредиенты к пицце</h1>
        </div>
    </div>



    <div class="row" style="padding-top: 1em; padding-bottom: 1em;">
        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Пармезан
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Шампиньоны
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">60 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Моцарелла
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">60 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Помидоры
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">60 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Лук обжаренный
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>



        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Корнишон
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Черри
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">100 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Бекон
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Курица копченная
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">60 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Салями
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">60 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Ветчина
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">60 р.</span>
                </div>
            </div>
        </div>



        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Тигровые креветки
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">80 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Морской коктейль
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Угорь
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">90 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Лосось
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">80 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Мидии
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">80 р.</span>
                </div>
            </div>
        </div>



        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Мясное ассорти
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">80 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Фарш
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Охотничьи колбаски
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Халапеньо
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">70 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Зелень микс
                </div>
                <div class="card-body">
                    <span class="text-primary">50  г</span> / <span class="text-danger">80 р.</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col text-center">
            Все дополнительные ингредиенты из приведенного набора<br>
            вы можете заказать в добавок к пицце
        </div>
    </div>


@endsection
