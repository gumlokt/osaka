@extends('layouts.app')

@section('content')
    <div class="row" style="padding-top: 1em;">
        <div class="col text-center">
            <h1 class="display-4">Акции</h1>
        </div>
    </div>
    <div class="row" style="padding-top: 1em; padding-bottom: 2em;">
        <div class="col text-center">
            <img src="/images/menu/actions/action01.jpg" class="img-fluid" alt="action01">
        </div>
    </div>
@endsection
