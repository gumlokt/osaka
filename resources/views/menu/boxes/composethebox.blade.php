@extends('layouts.app')

@section('content')
    <div class="row" style="padding-top: 1em;">
        <div class="col text-center">
            <h1 class="display-4">Лапша WOK</h1>
        </div>
    </div>

    <div class="row" style="padding-top: 1em; padding-bottom: 2em;">
        <div class="col text-center">
            <h2 class="display-4" style="font-size: 2.5rem; color: #f00;">Уважаемые клиенты!</h2>
            <h3 class="display-4" style="font-size: 1.2rem;">Из-за роста цен на сырье мы вынуждены включать стоимость упаковки в цену заказов</h3>
            <h3 class="display-4" style="font-size: 1.2rem;">Если заказ сформирован из раздела "Роллы", то стоимость упаковки составит <span class="text-danger">90 р.</span></h3>
            <h3 class="display-4" style="font-size: 1.2rem;">Если заказ сформирован из разделов "Пицца" и "Роллы", то стоимость упаковки составит <span class="text-danger">180 р.</span></h3>
            <h3 class="display-4" style="font-size: 1.2rem;">Если заказ сформирован из разделов "Пицца", "Роллы" и любоого другого раздела, то стоимость упаковки составит <span class="text-danger">270 р.</span></h3>
            <h3 class="display-4" style="font-size: 1.2rem;">При заказе бургера, Чикен Дога или шаурмы стоимость упаковки составит <span class="text-danger">35 р.</span></h3>
            <h3 class="display-4" style="font-size: 1.2rem;">При заказе от 5 пицц и более стоимость упаковки составит <span class="text-danger">400 р.</span></h3>
        </div>
    </div>

    <div class="row" style="padding-top: 1em;">
        <div class="col text-center">
            <h2 class="display-4">Собери свою коробочку</h2>
        </div>
    </div>

    <div class="row" style="padding-top: 1em; padding-bottom: 2em;">
        <div class="col text-center">
            <img src="/images/menu/boxes/composethebox.jpg" class="img-fluid" alt="composethebox">
        </div>
    </div>

    <div class="row" style="padding-top: 1em; padding-bottom: 2em;">
        <div class="col text-center">
            <h2 class="display-4" style="font-size: 2.5rem;">ДЕЙСТВИЕ ДИСКОНТНЫХ КАРТ, АКЦИЙ И СКИДОК НА ЛАПШУ WOK</h2>
            <h2 class="display-4" style="font-size: 2.5rem; color: #f00;">НЕ РАСПРОСТРАНЯЕТСЯ</h2>
        </div>
    </div>

    <div class="row" style="padding-top: 1em; padding-bottom: 2em;">
        <div class="col text-center">
            <img src="/images/menu/boxes/box-01.jpg" class="img-fluid" alt="box">
        </div>

        <div class="col text-center">
            <img src="/images/menu/boxes/box-03.jpg" class="img-fluid" alt="box">
        </div>

        <div class="col text-center">
            <img src="/images/menu/boxes/box-04.jpg" class="img-fluid" alt="box">
        </div>

        <div class="col text-center">
            <img src="/images/menu/boxes/box-02.jpg" class="img-fluid" alt="box">
        </div>
    </div>

    <div class="row" style="padding-top: 1em; padding-bottom: 2em;">
        <div class="col text-center">
            <h3 class="display-5" style="color:#f00;">Внешний вид блюд может отличаться от представленного на фотографиях</h3>
        </div>
    </div>
@endsection
