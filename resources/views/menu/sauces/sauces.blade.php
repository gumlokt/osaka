@extends('layouts.app')

@section('content')
    <div class="row" style="padding-top: 1em;">
        <div class="col text-center">
            <h1 class="display-4">Соусы</h1>
        </div>
    </div>



    <div class="row" style="padding-top: 1em; padding-bottom: 1em;">
        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Кетчуп
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Тар-тар
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Чесночный
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Ореховый
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Цезарь
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Табаско
                </div>
                <div class="card-body">
                    <span class="text-primary">15  г</span> / <span class="text-danger">50 р.</span>
                </div>
            </div>
        </div>



        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Шрирачи
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Спайси
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Барбекю
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Терияки
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Сладкий чили
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Баварский
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>



        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.1em;">
                    Сальса жгуче-острый
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary" style="padding: 0.75rem 0.3em;">
                    Кисло-сладкий
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Сырный
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-xl-2 mb-4">
            <div class="card border-primary text-center">
                <div class="card-header text-white bg-primary">
                    Кари
                </div>
                <div class="card-body">
                    <span class="text-primary">20  г</span> / <span class="text-danger">20 р.</span>
                </div>
            </div>
        </div>
    </div>


@endsection
