<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'IndexController@index');

// New dishes
Route::get('/news', 'NewsController@news');

// Business lunch
Route::get('/businesslunch', 'BusinesslunchController@index');

// Sushi
Route::get('/sushi', 'SushiController@sushi');
Route::get('/allforsushi', 'SushiController@allforsushi');

// Rolls
Route::get('/coldrolls', 'RollController@coldrolls');
Route::get('/friedandbakedrolls', 'RollController@friedandbakedrolls');
Route::get('/brandedrolls', 'RollController@brandedrolls');
Route::get('/sets', 'RollController@sets');
Route::get('/allforrolls', 'RollController@allforrolls');

// Pizza
Route::get('/pizza', 'PizzaController@pizza');
Route::get('/ingredientsforpizza', 'PizzaController@ingredientsforpizza');

// Boxes
Route::get('/boxes', 'BoxController@composethebox');

// Shashlik
Route::get('/shashlik', 'ShashlikController@shashlik');

// Pasta
Route::get('/pasta', 'PastaController@pasta');

// Fast food
Route::get('/streetfood', 'StreetfoodController@streetfood');

// Salads
Route::get('/salads', 'SaladController@salads');

// Pies
Route::get('/pies', 'PieController@pies');

// Sauces
Route::get('/sauces', 'SauceController@sauces');

// Drinks
Route::get('/drinks', 'DrinkController@drinks');

// Actions
Route::get('/actions', 'ActionController@actions');


// Contacts
Route::get('contacts', 'ContactController@contacts');
Route::post('sendmessage', 'MessageController@send');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
